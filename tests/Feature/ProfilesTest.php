<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfilesTest extends TestCase
{
    use RefreshDatabase;

    public function testAUserHasAProfile()
    {
        $user = create('App\User');

        $this->get("/profiles/{$user->user_name}")
            ->assertSee($user->user_name);

//        $this->withExceptionHandling()
//            ->post('replies/1/favorites')
//            ->assertRedirect('/login');
    }

    public function testProfilesDisplayAllThreadsCreatedByTheAssociatedUser()
    {
        $this->signIn();

        $thread = create('App\Thread', ['user_id' => auth()->id()]);

        $this->get("/profiles/" . auth()->user()->user_name)
            ->assertSee($thread->title)
            ->assertSee($thread->body);

    }

}