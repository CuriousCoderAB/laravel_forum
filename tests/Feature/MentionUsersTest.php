<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMentionedUsersInAReplyAreNotified()
    {
        $johnDoe = create('App\User', ['user_name' => 'JohnDoe']);

        $this->signIn($johnDoe);

        $janeDoe = create('App\User', ['user_name' => 'JaneDoe']);

        $thread = create('App\Thread');

        $reply = make('App\Reply', [
            'body' => '@JaneDoe and @FrankDoe, look at this!'
        ]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray());

        $this->assertCount(1, $janeDoe->notifications);

    }

    public function testItCanFetchAllMentionedUsersStartingWithTheGivenCharacters()
    {
        create('App\User', ['user_name' => 'johndoe']);
        create('App\User', ['user_name' => 'johnbill']);
        create('App\User', ['user_name' => 'janedoe']);

        $results = $this->json('GET', '/api/users', ['user_name' => 'john']);

        $this->assertCount(2, $results->json());
    }
}
