# LaravelForum

Prerequisites

- PHP 7.
- Recaptcha Keys
- SMTP Credentials (Mailhog, mailtrap etc)
- Algolia keys 

git clone git@bitbucket.org:CuriousCoderAB/laravel_forum.git laravelforum
cd laravelforum && composer install && npm install
php artisan app:setup
npm run dev 
