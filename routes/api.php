<?php

use Illuminate\Http\Request;

Route::get('/users', 'Api\UsersController@index');

Route::group([
    'middleware' => ['auth:api']
], function () {
    Route::post('/users/{user}/avatar', 'Api\UserAvatarController@store')->name('avatar');
});

