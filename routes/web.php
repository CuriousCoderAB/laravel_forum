<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/home', 'HomeController@show')->name('home');

Route::view('scan', 'scan');

//Route::resource('threads', 'ThreadsController');

Route::group([
    'middleware' => ['admin']
], function () {
    Route::post('locked-threads/{thread}', 'LockedThreadsController@store')->name('locked-threads.store');
    Route::delete('locked-threads/{thread}', 'LockedThreadsController@destroy')->name('locked-threads.destroy');
});

Route::group([
    'prefix' => 'threads',
], function () {
    Route::get('search', 'SearchController@show');

    Route::get('/', 'ThreadsController@index')->name('threads');
    Route::get('{channel}/{thread}', 'ThreadsController@show');
    Route::patch('{channel}/{thread}', 'ThreadsController@update')->name('threads.update');
    Route::delete('{channel}/{thread}', 'ThreadsController@destroy');
    Route::get('{channel}', 'ThreadsController@index');

    Route::get('{channel}/{thread}/replies', 'RepliesController@index');

    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::post('/{channel}/{thread}/subscriptions', 'ThreadSubscriptionsController@store');
        Route::delete('/{channel}/{thread}/subscriptions', 'ThreadSubscriptionsController@destroy');
        Route::post('{channel}/{thread}/replies', 'RepliesController@store');
    });
    Route::group([
        'middleware' => ['must-be-confirmed']
    ], function () {
        Route::post('/', 'ThreadsController@store')->middleware('must-be-confirmed');
        Route::get('create', 'ThreadsController@create')->middleware('must-be-confirmed');
    });
});

Route::group([
    'prefix' => 'replies',
    'middleware' => ['auth']
], function () {
    Route::delete('/{reply}', 'RepliesController@destroy')->name('replies.destroy');
    Route::patch('/{reply}', 'RepliesController@update');
    Route::post('/{reply}/favorites', 'FavoritesController@store');
    Route::delete('/{reply}/favorites', 'FavoritesController@destroy');
});

Route::post('/replies/{reply}/best', 'BestRepliesController@store')->name('best-replies.store');

Route::group([
    'prefix' => 'profiles',
    'middleware' => ['auth']
], function () {

    Route::get('/{user}', 'ProfilesController@show')->name('profile');

    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/{user}/notifications', 'UserNotificationsController@index');
        Route::delete('/{user}/notifications/{notification}', 'UserNotificationsController@destroy');
    });
});

Route::get('/register/confirm', 'Auth\RegisterConfirmationController@index')->name('register.confirm');
