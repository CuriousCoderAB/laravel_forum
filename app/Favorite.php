<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model
{
    use RecordsActivity, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function favorited()
    {
        return $this->morphTo();
    }
}
