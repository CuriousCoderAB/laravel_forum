<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $search = request('user_name');

        return User::where('user_name', 'LIKE', "$search%")
            ->take(5)
            ->pluck('user_name');
    }
}
