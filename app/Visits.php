<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2018-04-18
 * Time: 7:38 AM
 */

namespace App;


use Illuminate\Support\Facades\Redis;

class Visits
{
    protected $relationship;

    public function record()
    {
        Redis::incr($this->cacheKey());

        return $this;
    }

    public function __construct($relationship)
    {
        $this->relationship = $relationship;
    }

    public function reset()
    {
        Redis::del($this->cacheKey());

        return $this;
    }

    public function count()
    {
        return Redis::get($this->cacheKey()) ?? 0;
    }

    protected function cacheKey()
    {
        return "threads.{$this->relationship->id}.visits";
    }
}