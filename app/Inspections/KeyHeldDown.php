<?php
/**
 * Created by PhpStorm.
 * User: Andrew
 * Date: 2018-04-08
 * Time: 1:19 PM
 */

namespace App\Inspections;

use Exception;

class KeyHeldDown
{
    public function detect($body)
    {
        if(preg_match('/(.)\\1{4,}/', $body)) {
            throw new Exception('Your reply contains spam.');
        };
    }
}