<div class="card mb-3" v-if="editing">
    <div class="card-header">
        <div class="level">
            <input type="text" v-model="form.title" class="form-control"/>
        </div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <wysiwyg name="body" v-model="form.body"></wysiwyg>
        </div>
    </div>

    <div class="card-footer">
        <div class="level">
            <button class="btn btn-primary btn-sm" @click="update">Update</button>
            <button class="btn btn-sm level-item" @click="resetForm">Cancel</button>
            @can ('update', $thread)
                <form action="{{ $thread->path() }}" method="POST" class="ml-auto">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-link">Delete Thread</button>
                </form>
            @endcan
        </div>
    </div>
</div>

<div class="card mb-3" v-else>
    <div class="card-header">
        <div class="level">
            <img src="{{  $thread->creator->avatar_path }}" alt="{{ $thread->creator->user_name }}" class="mr-1" width="50" height="50"/>
            <span class="flex">
                <a href="{{ route('profile', $thread->creator->user_name) }}">
                    {{ $thread->creator->user_name }}
                </a>
                posted:
                <span v-text="title"></span>
            </span>
        </div>
    </div>

    <div class="card-body" v-html="body"></div>

    <div class="card-footer" v-if="authorize('owns', thread)">
        <button class="btn btn-sm" @click="editing = true">Edit</button>
    </div>
</div>