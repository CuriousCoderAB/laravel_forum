@extends('layouts.app')
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header p">
                	Create a New Thread
                </div>

                <div class="panel-body p-1">
                	<form method="POST" action="/threads">

                		{{ csrf_field() }}

                		<div class="form-group p">
                			<label for="channel_id">Choose a Channel</label>
                		    <select name="channel_id" id="channel_id" class="form-control" required>

                		    	<option value="">
                		    		Choose a channel...
                		    	</option>

                		    	@foreach ($channels as $channel)

                		    	<option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : ''}}>
                		    		{{ $channel->name }}
                		    	</option>

                		    	@endforeach
                		    </select>
                		</div>
                	
                		<div class="form-group p">
                			<label for="title">Title</label>
                		    <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" required>
                		</div>

                		<div class="form-group p">
                			<label for="body">Body</label>
                            <wysiwyg name="body"></wysiwyg>
                		</div>

                        <div class="form-group p">
                            <div class="g-recaptcha" data-sitekey="{{ config('services.recaptcha.key') }}"></div>
                        </div>

                		<div class="form-group p">
                			<button type="submit" class="btn btn-default">Submit</button>
                		</div>

                	</form>

                	@if (count($errors))

                		<ul class="alert alert-danger">
                			@foreach ($errors->all() as $error)
	                			<li>{{ $error }}</li>
                			@endforeach
                		</ul>

                	@endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection