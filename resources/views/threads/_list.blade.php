@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@forelse ($threads as $thread)
    <div class="card mb-3">
        <div class="card-header">
            <div class="level">
                <div class="flex">
                    <h4>
                        <a href="{{ $thread->path() }}">
                            @if (auth()->check() && $thread->hasUpdatesFor(auth()->user()))
                                <strong>
                                    {{ $thread->title }}
                                </strong>
                            @else
                                {{ $thread->title }}
                            @endif
                        </a>
                    </h4>

                    <h5>
                        Posted By: <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->user_name }}</a>
                    </h5>

                </div>

                <a href="{{ $thread->path() }}">
                    <strong>{{ $thread->replies_count }} {{str_plural('reply', $thread->replies_count) }}</strong>
                </a>

            </div>
        </div>
        <div class="card-body">
            <div class="body">
                {!! $thread->body !!}
            </div>
        </div>
        <div class="card-footer">
            {{ $thread->visits_count }} Visits
        </div>
    </div>
@empty
    <p>There are no relevant results at this time.</p>
@endforelse