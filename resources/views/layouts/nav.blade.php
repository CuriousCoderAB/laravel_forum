<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Andrew\'s Forum') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

                <li class="dropdown p-1">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Browse
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu p-1" aria-labelledby="dropdownMenu1">
                        <li>
                            <a class="nav-link" href="/threads">
                                All Threads
                            </a>
                        </li>

                        @auth

                            <li>
                                <a class="nav-link" href="/threads?by={{ auth()->user()->user_name }}">
                                    My Threads
                                </a>
                            </li>

                        @endauth

                        <li>
                            <a class="nav-link" href="/threads?popular=1">
                                Popular Threads of All Time
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="/threads?unanswered=1">
                                Unanswered Threads
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="d-flex justify-content-center align-content-center align-items-center">
                    <a class="nav-link attention d-flex justify-content-center align-content-center align-items-center" href="/threads/create">
                        New Thread
                    </a>
                </li>

                <li class="dropdown p-1">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Channels
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                        @foreach ($channels as $channel)

                            <li class="p-1">
                                <a href="/threads/{{ $channel->slug }}">
                                    {{ $channel->name }}
                                </a>
                            </li>

                        @endforeach

                    </ul>
                </li>

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                    <li><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                @else
                    <user-notifications></user-notifications>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->user_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('profile', Auth::user()) }}">
                                My Profile
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
