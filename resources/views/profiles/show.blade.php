@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-wrap">
                            <div class="">
                                <avatar-form :user="{{ $profileUser }}"></avatar-form>
                            </div>
                            <div class="">
                                <h1>{{ $profileUser->user_name }}</h1>
                                <h2>{{ $profileUser->fullName() }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @forelse ($activities as $date => $activity)
                    <h3 class="page-header">{{ $date }}</h3>
                    @foreach ($activity as $record)
                        @if (view()->exists("profiles.activities.{$record->type}"))
                            @include ("profiles.activities.{$record->type}", ['activity' => $record])
                        @endif
                    @endforeach
                @empty
                    <p>There is no activity yet, get out there and start discovering the world!</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
