@component('profiles.activities.activity')

    @slot('heading')
        <a href="{{ $activity->subject->favorited->path() }}">
            {{ $profileUser->user_name }} favorited a reply
        </a>
    @endslot

    @slot('body')
        {{ $activity->subject->favorited->body }}
    @endslot

@endcomponent